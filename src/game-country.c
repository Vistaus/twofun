/* Copyright 2018-2021 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "game-country.h"

#include <glib/gi18n.h>

typedef struct {
  GtkWidget *country1;
  GtkWidget *country2;
  GtkWidget *capital1;
  GtkWidget *capital2;

  GRand *rand;
  gboolean valid;
} TwoFunGameCountryPrivate;

struct _TwoFunGameCountry {
  TwoFunGame parent_instance;

  /*< private >*/
  TwoFunGameCountryPrivate *priv;
};

G_DEFINE_TYPE_WITH_PRIVATE (TwoFunGameCountry, twofun_game_country, TWOFUN_TYPE_GAME)

struct country {
  gchar *name;
  gchar *capital;
};

static struct country countries[] = {
  {N_("Afghanistan"), "Kabul"},
  {N_("Ägypten"), "Kairo"},
  {N_("Albanien"), "Tirana"},
  {N_("Algerien"), "Algier"},
  {N_("Andorra"), "Andorra la Vella"},
  {N_("Angola"), "Luanda"},
  {N_("Antigua und Barbuda"), "St. John's"},
  {N_("Äquatorialguinea"), "Malabo"},
  {N_("Argentinien"), "Buenos Aires"},
  {N_("Armenien"), "Jerewan"},
  {N_("Aserbaidschan"), "Baku"},
  {N_("Äthiopien"), "Addis Abeba"},
  {N_("Australien"), "Canberra"},
  {N_("Bahamas"), "Nassau"},
  {N_("Bahrain"), "Manama"},
  {N_("Bangladesch"), "Dhaka"},
  {N_("Barbados"), "Bridgetown"},
  {N_("Belgien"), "Brüssel"},
  {N_("Belize"), "Belmopan"},
  {N_("Benin"), "Porto Novo"},
  {N_("Bhutan"), "Thimphu"},
  {N_("Bolivien"), "Sucre"},
  {N_("Bosnien und Herzegowina"), "Sarajevo"},
  {N_("Botsuana"), "Gaborone"},
  {N_("Brasilien"), "Brasilia"},
  {N_("Brunei"), "Bandar Seri Begawan"},
  {N_("Bulgarien"), "Sofia"},
  {N_("Burkina Faso"), "Ouagadougou"},
  {N_("Burundi"), "Bujumbura"},
  {N_("Chile"), "Santiago de Chile"},
  {N_("China"), "Peking"},
  {N_("Costa Rica"), "San Jose"},
  {N_("Dänemark"), "Kopenhagen"},
  {N_("Deutschland"), "Berlin"},
  {N_("Dominica"), "Roseau"},
  {N_("Dominikanische Republik"), "Santo Domingo"},
  {N_("Dschibuti"), "Dschibuti"},
  {N_("Ecuador"), "Quito"},
  {N_("El Salvador"), "San Salvador"},
  {N_("Elfenbeinküste"), "Yamoussoukro"},
  {N_("Eritrea"), "Asmara"},
  {N_("Estland"), "Tallinn"},
  {N_("Fidschi"), "Suva"},
  {N_("Finnland"), "Helsinki"},
  {N_("Frankreich"), "Paris"},
  {N_("Gabun"), "Libreville"},
  {N_("Gambia"), "Banjul"},
  {N_("Georgien"), "Tiflis"},
  {N_("Ghana"), "Accra"},
  {N_("Grenada"), "St. George's"},
  {N_("Griechenland"), "Athen"},
  {N_("Guatemala"), "Guatemala-Stadt"},
  {N_("Guinea"), "Conakry"},
  {N_("Guinea-Bissau"), "Bissau"},
  {N_("Guyana"), "Georgetown"},
  {N_("Haiti"), "Port-au-Prince"},
  {N_("Honduras"), "Tegucigalpa"},
  {N_("Indien"), "Neu-Delhi"},
  {N_("Indonesien"), "Jakarta"},
  {N_("Irak"), "Bagdad"},
  {N_("Iran"), "Teheran"},
  {N_("Irland"), "Dublin"},
  {N_("Island"), "Reykjavik"},
  {N_("Israel"), "Jerusalem"},
  {N_("Italien"), "Rom"},
  {N_("Jamaika"), "Kingston"},
  {N_("Japan"), "Tokio"},
  {N_("Jemen"), "Sanaa"},
  {N_("Jordanien"), "Amman"},
  {N_("Kambodscha"), "Phnom Penh"},
  {N_("Kamerun"), "Jaunde"},
  {N_("Kanada"), "Ottawa"},
  {N_("Kap Verde"), "Praia"},
  {N_("Kasachstan"), "Astana"},
  {N_("Katar"), "Doha"},
  {N_("Kenia"), "Nairobi"},
  {N_("Kirgisistan"), "Bischkek"},
  {N_("Kiribati"), "South Tarawa"},
  {N_("Kolumbien"), "Bogota"},
  {N_("Komoren"), "Moroni"},
  {N_("Kongo, Demokratische Republik"), "Kinshasa"},
  {N_("Kongo, Republik"), "Brazzaville"},
  {N_("Korea Nord"), "Pjöngjang"},
  {N_("Korea Süd"), "Seoul"},
  {N_("Kosovo"), "Pristina"},
  {N_("Kroatien"), "Zagreb"},
  {N_("Kuba"), "Havanna"},
  {N_("Kuwait"), "Kuwait-Stadt"},
  {N_("Laos"), "Vientiane"},
  {N_("Lesotho"), "Maseru"},
  {N_("Lettland"), "Riga"},
  {N_("Libanon"), "Beirut"},
  {N_("Liberia"), "Monrovia"},
  {N_("Libyen"), "Tripolis"},
  {N_("Lichtenstein"), "Vaduz"},
  {N_("Litauen"), "Vilnius"},
  {N_("Luxemburg"), "Luxemburg"},
  {N_("Madagaskar"), "Antananarivo"},
  {N_("Malawi"), "Lilongwe"},
  {N_("Malaysia"), "Kuala Lumpur"},
  {N_("Malediven"), "Male"},
  {N_("Mali"), "Bamako"},
  {N_("Malta"), "Valletta"},
  {N_("Marokko"), "Rabat"},
  {N_("Marshallinseln"), "Majuro"},
  {N_("Mauretanien"), "Nouakchott"},
  {N_("Mauritius"), "Port Louis"},
  {N_("Mazedonien"), "Skopje"},
  {N_("Mexiko"), "Mexiko-Stadt"},
  {N_("Mikronesien"), "Palikir"},
  {N_("Moldawien"), "Chisinau"},
  {N_("Monaco"), "Monaco"},
  {N_("Mongolei"), "Ulaanbaatar"},
  {N_("Montenegro"), "Podgorica"},
  {N_("Mosambik"), "Maputo"},
  {N_("Myanmar"), "Pyinmana Naypyidaw"},
  {N_("Namibia"), "Windhoek"},
  {N_("Nauru"), "Yaren"},
  {N_("Nepal"), "Kathmandu"},
  {N_("Neuseeland"), "Wellington"},
  {N_("Nicaragua"), "Managua"},
  {N_("Niederlande"), "Amsterdam"},
  {N_("Niger"), "Niamey"},
  {N_("Nigeria"), "Abuja"},
  {N_("Norwegen"), "Oslo"},
  {N_("Oman"), "Maskat"},
  {N_("Österreich"), "Wien"},
  {N_("Osttimor"), "Dili"},
  {N_("Pakistan"), "Islamabad"},
  {N_("Palau"), "Melekeok"},
  {N_("Panama"), "Panama-Stadt"},
  {N_("Papua-Neuguinea"), "Port Moresby"},
  {N_("Paraguay"), "Asuncion"},
  {N_("Peru"), "Lima"},
  {N_("Philippinen"), "Manila"},
  {N_("Polen"), "Warschau"},
  {N_("Portugal"), "Lissabon"},
  {N_("Ruanda"), "Kigali"},
  {N_("Rumänien"), "Bukarest"},
  {N_("Russland"), "Moskau"},
  {N_("Salomonen"), "Honiara"},
  {N_("Sambia"), "Lusaka"},
  {N_("Samoa"), "Apia"},
  {N_("San Marino"), "San Marino"},
  {N_("Saudi-Arabien"), "Riad"},
  {N_("Schweden"), "Stockholm"},
  {N_("Schweiz"), "Bern"},
  {N_("Senegal"), "Dakar"},
  {N_("Serbien"), "Belgrad"},
  {N_("Seychellen"), "Victoria"},
  {N_("Sierra Leone"), "Freetown"},
  {N_("Simbabwe"), "Harare"},
  {N_("Singapur"), "Singapur"},
  {N_("Slowakei"), "Bratislava"},
  {N_("Slowenien"), "Ljubljana"},
  {N_("Somalia"), "Mogadischu"},
  {N_("Spanien"), "Madrid"},
  {N_("Sri Lanka"), "Colombo"},
  {N_("St. Kitts und Nevis"), "Basseterre"},
  {N_("Sl. Lucia"), "Castries"},
  {N_("St. Vincent und die Grenadinen"), "Kingstown"},
  {N_("Südafrika"), "Pretoria"},
  {N_("Sudan"), "Khartum"},
  {N_("Südsudan"), "Juba"},
  {N_("Suriname"), "Paramaribo"},
  {N_("Swasiland"), "Mbabane"},
  {N_("Syrien"), "Damaskus"},
  {N_("Sao Tome und Principe"), "Sao Tome"},
  {N_("Tadschikistan"), "Duschanbe"},
  {N_("Tansania"), "Dodoma"},
  {N_("Thailand"), "Bangkok"},
  {N_("Togo"), "Lome"},
  {N_("Tonga"), "Nuku'alofa"},
  {N_("Trinidad und Tobago"), "Port-of-Spain"},
  {N_("Tschad"), "N'Djamena"},
  {N_("Tschechien"), "Prag"},
  {N_("Tunesien"), "Tunis"},
  {N_("Türkei"), "Ankara"},
  {N_("Turkmenistan"), "Aschgabat"},
  {N_("Tuvalu"), "Funafuti"},
  {N_("Uganda"), "Kampala"},
  {N_("Ukraine"), "Kiew"},
  {N_("Ungarn"), "Budapest"},
  {N_("Uruguay"), "Montevideo"},
  {N_("Usbekistan"), "Taschkent"},
  {N_("Vanuatu"), "Port Vila"},
  {N_("Venezuela"), "Caracas"},
  {N_("Vereinigte Arabische Emirate"), "Abu Dhabi"},
  {N_("Vereinigte Staaten von Amerika"), "Washington"},
  {N_("Vereinigtes Königreich"), "London"},
  {N_("Vietnam"), "Hanoi"},
  {N_("Weißrussland"), "Minsk"},
  {N_("Zentralafrikanische Republik"), "Bangui"},
  {N_("Zypern"), "Nikosia"},
};

static gsize countries_len = G_N_ELEMENTS (countries);

gchar *
twofun_game_country_get_description (TwoFunGame *game)
{
  return g_strdup (_("Country / Capital?"));
}

void
twofun_game_country_next_step (TwoFunGame *game)
{
  TwoFunGameCountryPrivate *priv = twofun_game_country_get_instance_private (TWOFUN_GAME_COUNTRY (game));
  PangoAttrList *attr_list = NULL;
  PangoAttribute *attr = NULL;
  static int old_idx = 0;
  int idx;
  int col_idx = 0;

  do {
    idx = g_get_monotonic_time () % countries_len;
  } while (idx == old_idx);

  old_idx = idx;
  col_idx = idx;

  if (!(g_get_monotonic_time () % 2)) {
    col_idx++;
    col_idx %= countries_len;
  }

  attr_list = pango_attr_list_new ();
  attr = pango_attr_scale_new (1.5);
  pango_attr_list_insert (attr_list, attr);
  attr = pango_attr_weight_new (PANGO_WEIGHT_SEMIBOLD);
  pango_attr_list_insert (attr_list, attr);

  gtk_label_set_text (GTK_LABEL (priv->country1), _(countries[idx].name));
  gtk_label_set_attributes (GTK_LABEL (priv->country1), attr_list);

  gtk_label_set_text (GTK_LABEL (priv->country2), _(countries[idx].name));
  gtk_label_set_attributes (GTK_LABEL (priv->country2), attr_list);

  attr_list = pango_attr_list_new ();
  attr = pango_attr_scale_new (1.5);
  pango_attr_list_insert (attr_list, attr);
  attr = pango_attr_weight_new (PANGO_WEIGHT_SEMIBOLD);
  pango_attr_list_insert (attr_list, attr);

  gtk_label_set_text (GTK_LABEL (priv->capital1), _(countries[col_idx].capital));
  gtk_label_set_attributes (GTK_LABEL (priv->capital1), attr_list);

  gtk_label_set_text (GTK_LABEL (priv->capital2), _(countries[col_idx].capital));
  gtk_label_set_attributes (GTK_LABEL (priv->capital2), attr_list);

  priv->valid = col_idx == idx;
  g_signal_emit_by_name (TWOFUN_GAME (game), "progress-activated", 2.0f);
}

gboolean
twofun_game_country_is_valid (TwoFunGame *game)
{
  TwoFunGameCountryPrivate *priv = twofun_game_country_get_instance_private (TWOFUN_GAME_COUNTRY (game));

  return priv->valid;
}
static void
twofun_game_country_class_init (TwoFunGameCountryClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  TwoFunGameClass *game_class = TWOFUN_GAME_CLASS (klass);

  game_class->get_description = twofun_game_country_get_description;
  game_class->next_step = twofun_game_country_next_step;
  game_class->is_valid = twofun_game_country_is_valid;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/twofun/game-country.ui");

  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameCountry, country1);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameCountry, country2);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameCountry, capital1);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameCountry, capital2);
}

static void
twofun_game_country_init (TwoFunGameCountry *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWidget *
twofun_game_country_new (void)
{
  return g_object_new (TWOFUN_TYPE_GAME_COUNTRY,
                       NULL);
}
