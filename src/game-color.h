/* Copyright 2018-2021 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <glib.h>

#include "game.h"

G_BEGIN_DECLS

#define TWOFUN_TYPE_GAME_COLOR (twofun_game_color_get_type())

G_DECLARE_FINAL_TYPE(TwoFunGameColor, twofun_game_color, TWOFUN_GAME, COLOR, TwoFunGame)

GtkWidget *twofun_game_color_new (void);

G_END_DECLS

