/* Copyright 2018-2021 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>
#include <gtk/gtk.h>

#include <adwaita.h>

#define TWOFUN_TYPE_GAME (twofun_game_get_type())
G_DECLARE_DERIVABLE_TYPE (TwoFunGame, twofun_game, TWOFUN, GAME, GtkBox)

G_BEGIN_DECLS

struct _TwoFunGameClass
{
  /*< private >*/
  GtkBoxClass   parent_class;

  gchar *(*get_description)(TwoFunGame *game);
  void   (*next_step)(TwoFunGame *game);
  gboolean (*is_valid)(TwoFunGame *game);
  gdouble (*get_game_time)(TwoFunGame *game);
};

gchar *twofun_game_get_description (TwoFunGame *game);
void twofun_game_next_step (TwoFunGame *game);
gboolean twofun_game_is_valid (TwoFunGame *game);
gdouble twofun_game_get_game_time (TwoFunGame *game);

G_END_DECLS
