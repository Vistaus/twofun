/* Copyright 2018-2021 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "game-color.h"

#include <glib/gi18n.h>

typedef struct {
  GtkWidget *label1;
  GtkWidget *label2;

  GRand *rand;
  gboolean valid;
} TwoFunGameColorPrivate;

struct _TwoFunGameColor {
  TwoFunGame parent_instance;

  /*< private >*/
  TwoFunGameColorPrivate *priv;
};

G_DEFINE_TYPE_WITH_PRIVATE (TwoFunGameColor, twofun_game_color, TWOFUN_TYPE_GAME)

struct color {
  gchar *name;
  guint16 red;
  guint16 green;
  guint16 blue;
};

static struct color colors[] = {
  {N_("BLACK"), 0x0000, 0x0000, 0x0000},
  {N_("RED"), 0xffff, 0x0000, 0x0000},
  {N_("GREEN"), 0x0000, 0xffff, 0x0000},
  {N_("BLUE"), 0x0000, 0x0000, 0xffff},
  {N_("PURPLE"), 0x8000, 0x0000, 0x8000},
  {N_("CYAN"), 0x0000, 0xffff, 0xffff},
  {N_("YELLOW"), 0xffff, 0xffff, 0x0000},
  {N_("ORANGE"), 0xffff, 0x8000, 0x0000}
};

static gsize colors_len = G_N_ELEMENTS (colors);

gchar *
twofun_game_color_get_description (TwoFunGame *game)
{
  return g_strdup (_("Color = Word?"));
}

void
twofun_game_color_next_step (TwoFunGame *game)
{
  TwoFunGameColorPrivate *priv = twofun_game_color_get_instance_private (TWOFUN_GAME_COLOR (game));
  PangoAttrList *attr_list = NULL;
  PangoAttribute *attr = NULL;
  static int old_idx = 0;
  int idx;
  int col_idx = 0;

  do {
    idx = g_get_monotonic_time () % colors_len;
  } while (idx == old_idx);

  old_idx = idx;
  col_idx = idx;

  if (!(g_get_monotonic_time () % 2)) {
    col_idx++;
    col_idx %= colors_len;
  }

  attr_list = pango_attr_list_new ();
  attr = pango_attr_foreground_new (colors[col_idx].red, colors[col_idx].green, colors[col_idx].blue);
  pango_attr_list_insert (attr_list, attr);
  attr = pango_attr_scale_new (2.0);
  pango_attr_list_insert (attr_list, attr);
  attr = pango_attr_weight_new (PANGO_WEIGHT_SEMIBOLD);
  pango_attr_list_insert (attr_list, attr);

  gtk_label_set_text (GTK_LABEL (priv->label1), _(colors[idx].name));
  gtk_label_set_attributes (GTK_LABEL (priv->label1), attr_list);

  gtk_label_set_text (GTK_LABEL (priv->label2), _(colors[idx].name));
  gtk_label_set_attributes (GTK_LABEL (priv->label2), attr_list);

  priv->valid = col_idx == idx;
  g_signal_emit_by_name (TWOFUN_GAME (game), "progress-activated", 1.0f);
}

gboolean
twofun_game_color_is_valid (TwoFunGame *game)
{
  TwoFunGameColorPrivate *priv = twofun_game_color_get_instance_private (TWOFUN_GAME_COLOR (game));

  return priv->valid;
}

static void
twofun_game_color_class_init (TwoFunGameColorClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  TwoFunGameClass *game_class = TWOFUN_GAME_CLASS (klass);

  game_class->get_description = twofun_game_color_get_description;
  game_class->next_step = twofun_game_color_next_step;
  game_class->is_valid = twofun_game_color_is_valid;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/twofun/game-color.ui");

  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameColor, label1);
  gtk_widget_class_bind_template_child_private (widget_class, TwoFunGameColor, label2);
}

static void
twofun_game_color_init (TwoFunGameColor *self)
{
  TwoFunGameColorPrivate *priv = twofun_game_color_get_instance_private (self);

  gtk_widget_init_template (GTK_WIDGET (self));

  priv->rand = g_rand_new ();
}

GtkWidget *
twofun_game_color_new (void)
{
  return g_object_new (TWOFUN_TYPE_GAME_COLOR, NULL);
}
